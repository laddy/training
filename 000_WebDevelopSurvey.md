# Web Develop Survey (2015-07) Webに関するシェア調査

Webに関する調査報告

## Netcraft

http://www.netcraft.com/

### HostnameベースのWebsiteアクティブ数

![Webサーバホストネーム数](image/survey_00001.jpg)

### Webサーバ

![Webサーバ](image/survey_00002.jpg)

## w3techs

Web系のテクノロジーについて調査しているところ
[http://w3techs.com](http://w3techs.com/)

W3Techs.com, 23 July 2015

### Web Servers

| WebServer | Share |
| ---- | ---- |
| Apache | 56.9% |
| Nginx | 24.9% |
| Microsoft-IIS | 13.1% |
| LiteSpeed | 2.2% |
| Google Servers | 1.3% |

[Link](http://w3techs.com/technologies/overview/web_server/all)

### OS

| OS | Share |
| ---- | ---- |
| UNIX | 67.1% |
| Windows | 32.9% |
| Unix | Share |
| Linux  | 53.7% |
| BSD |  1.3% |
| Darwin less than |  0.1% |
| Solaris less than |  0.1% |
| HP-UX less than |  0.1% |
| Unknown  | 45.0% |


[Link](http://w3techs.com/technologies/overview/operating_system/all)

### Content languages

| Language | Share |
| ---- | ---- |
| English  |55.2% |
| Russian | 5.9% |
| German | 5.7% |
| Japanese | 5.0% |
| Spanish, Castilian | 4.6% |

[Link](http://w3techs.com/technologies/overview/content_language/all)

### Character Encodings

| Encoding | Share |
| ---- | ---- |
| UTF-8 | 84.5% |
| ISO-8859-1 | 7.8% |
| Windows-1251 | 2.0% |
| Shift JIS | 1.2% |
| GB2312 | 1.1% |

[Link](http://w3techs.com/technologies/overview/character_encoding/all)

### Server-side Languages

| PG | Share |
| ---- | ---- |
| PHP | 81.9% |
| ASP.NET | 16.8% |
| Java | 3.0% |
| ColdFusion | 0.7% |
| Ruby | 0.6% |

[Link](http://w3techs.com/technologies/overview/programming_language/all)

### Client-side Languages

| PG | Share |
| ---- | ---- |
| JavaScript  | 89.8% |
| Flash  | 10.5% |
| Silverlight |  0.1% |
| Java |  0.1% |

[Link](http://w3techs.com/technologies/overview/client_side_language/all)

### Javascript Libraries

| Libraries | Share |
| ---- | ---- |
| JQuery | 95.4% |
| Modernizr | 12.8% |
| Bootstrap | 12.2% |
| MooTools | 5.8% |
| ASP.NET Ajax | 3.4% |

[Link](http://w3techs.com/technologies/overview/javascript_library/all)

### Content Management System

| CMS | Share |
| ---- | ---- |
| WordPress | 60.4% |
| Joomla | 7.0% |
| Drupal | 5.1% |
| Magento | 2.9% |
| Blogger | 2.8% |

[Link](http://w3techs.com/technologies/overview/content_management/all)




# Git開発の進め方

WisemanWebチーム内でのやり方のため、
他とは違う、改善する部分が多々あると思います。

## 基本

チーム内の使い方

- Bitbucketにリポジトリを作成する
- リポジトリへ各種ライブラリ設置
- 各自pull & 開発
- ブランチの分け方
    - master => 開発確認
    - release => ユーザ向けリリース用
    - develop => 開発途中検証用
- 基本はmasterでの作業
- push時にHipchatへの通知 & 開発用サーバへ自動デプロイ
- リリース待ちになったブツは責任者がreleaseブランチへマージ
- マージされたreleaseを各種サーバへ手動デプロイ


## 開発の進め方

Bitbucketにリポジトリ作成
表示されている画像のようにプライベートリポジトリで作成する。

[![](image/652411aa-d2d7-47d7-9506-b9379f994bee.jpg)](image/652411aa-d2d7-47d7-9506-b9379f994bee.jpg)

リポジトリからcloneする

    $ git clone git@bitbucket.org:laddy/knowledge.git

cloneしたソースから開発を進める
機能的に一段落したと思ったらローカルでのcommitを行う

    $ git add .
    $ git commit -m "hogehoge"

リポジトリへpushする(基本masterブランチへ)

    $ git push origin master

多分誰かが開発用サーバに自動デプロイを仕込んでいると思うので確認を行う


## リリースブランチへの統合

リリースブランチへのチェックアウト

    $ git checkout release

現在操作中のブランチ確認

    $ git branch
    * release
    master

masterブランチから統合

    $ git pull origin master

リリース時のタグ設定

    $ git tag 20150401

リポジトリへreleaseブランチをpush

    $ git push origin release --tags



## これから実現したい

- チケット管理システム(Redmine)等との連携
- Releaseブランチからの自動デプロイ
- Jenkinsとの連携
    - PHPUnit
    - FrontEndTest
    - Selenium
    - テストがダメだったら自動デプロイしない等
- pull request による管理


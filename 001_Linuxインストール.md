# Linux インストール

## Linux image のDownload

- ubuntuからDL
- centos

## インストールの種類

### 物理的にインストールする

- ImageファイルをDVDへ焼く
- PCへDVD挿入
- DVDから起動する

### 仮想環境にインストールする

- VMの起動ディスクへイメージを挿入
- 仮想マシンを起動する

## Ubuntu

![install](./image/ubuntu_00001.jpg)

![install](./image/ubuntu_00002.jpg)

![install](./image/ubuntu_00003.jpg)

![install](./image/ubuntu_00004.jpg)

![install](./image/ubuntu_00005.jpg)

![install](./image/ubuntu_00006.jpg)

![install](./image/ubuntu_00007.jpg)

![install](./image/ubuntu_00008.jpg)

![install](./image/ubuntu_00009.jpg)

![install](./image/ubuntu_00010.jpg)

![install](./image/ubuntu_00011.jpg)

![install](./image/ubuntu_00012.jpg)

![install](./image/ubuntu_00013.jpg)

![install](./image/ubuntu_00014.jpg)

![install](./image/ubuntu_00015.jpg)

![install](./image/ubuntu_00016.jpg)

![install](./image/ubuntu_00017.jpg)

![install](./image/ubuntu_00018.jpg)

![install](./image/ubuntu_00019.jpg)

![install](./image/ubuntu_00020.jpg)



## CentOS


![install](./image/centos_00001.jpg)

![install](./image/centos_00002.jpg)

![install](./image/centos_00003.jpg)

![install](./image/centos_00004.jpg)

![install](./image/centos_00005.jpg)

![install](./image/centos_00006.jpg)

![install](./image/centos_00007.jpg)

![install](./image/centos_00008.jpg)

![install](./image/centos_00009.jpg)

![install](./image/centos_00010.jpg)

![install](./image/centos_00011.jpg)

![install](./image/centos_00012.jpg)

![install](./image/centos_00013.jpg)

![install](./image/centos_00014.jpg)

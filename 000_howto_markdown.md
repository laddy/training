# Markdownでドキュメントを書く

## Markdownとは

> Markdown（マークダウン）は、文書を記述するための軽量マークアップ言語のひとつである。もとはプレーンテキスト形式で手軽に書いた文書からHTMLを生成するために開発された。現在ではHTMLのほかパワーポイント形式やLATEX形式のファイルへ変換するソフトウェア（コンバータ）も開発されている。各コンバータの開発者によって多様な拡張が施されるため、各種の方言が存在する。

[https://ja.wikipedia.org/wiki/Markdown](https://ja.wikipedia.org/wiki/Markdown)

## Markdown利点

- HTMLより簡素で簡単なマークアップ
- Textベース(OS問わず記述ができる)
- GitHub, CMSなどのプラグインの対応が広い


## Howto Write?

### Format Text

#### Headers - 見出し

---- 
# Header1
## Header2
### Header3
#### Header4
----

#### Emphasis - 強調

_イタリック体1_ *イタリック体2*

__強調1__ **強調2**

#### Strikethrough - 打ち消し線

~~打ち消し線~~

#### List - リスト

##### Disk

文頭に「*」「+」「-」いずれかを入れる

* リスト1
* リスト2

##### Decimal

文頭に「数字.」を入れる

1. リスト1
1. リスト2
1. リスト3

##### Definition

<dl>
  <dt>リンゴ</dt>
  <dd>赤いフルーツ</dd>
  <dt>オレンジ</dt>
  <dd>橙色のフルーツ</dd>
</dl>

#### Blockquotes - 引用

> 以下はこの噺の主人公である赤ん坊に付けられる「名前」の一例である。日本で最も長い名前、としてしばしば語られる。

> 「じゅげむ　じゅげむ　ごこうのすりきれ　かいじゃりすいぎょの　すいぎょうまつ　うんらいまつ　ふうらいまつ　くうねるところにすむところ　やぶらこうじのぶらこうじ　ぱいぽ　ぱいぽ　ぱいぽのしゅーりんがん　しゅーりんがんのぐーりんだい　ぐーりんだいのぽんぽこぴーの　ぽんぽこなーの　ちょうきゅうめいのちょうすけ」

#### Horizontal rules - 水平線

----
* * *
***

#### Links - リンク

[寿限無](https://ja.wikipedia.org/wiki/%E5%AF%BF%E9%99%90%E7%84%A1)

[yahoo][link-1] [yahoo][link-1]

[link-1]:http://www.yahoo.co.jp

#### Images - 画像

![GNU](https://www.gnu.org/graphics/heckert_gnu.small.png "GNU")


#### Other - その他

##### エスケープ

バックスラッシュを入力すると特殊文字をエスケープする

- \# H1
- \\ バックスラッシュ

### Code block

```c
#include <stdio.h>

int main()
{
    int i = 0;

    printf("%d\n", i);
    return 1;
}
```

### Table

| Item      |    Value | Qty  |
| :-------- | --------:| :--: |
| Computer  | 1600 USD |  5   |
| Phone     |   12 USD |  12  |
| Pipe      |    1 USD | 234  |



## 表示が限られている記法

### LaTeX expressionj - TeXの数式

$$  x = \dfrac{-b \pm \sqrt{b^2 - 4ac}}{2a} $$


### Diagrams

#### Flow charts

```flow
st=>start: Start
e=>end
op=>operation: My Operation
cond=>condition: Yes or No?

st->op->cond
cond(yes)->e
cond(no)->op
```

#### Sequence diagrams 

```sequence
Alice->Bob: Hello Bob, how are you?
Note right of Bob: Bob thinks
Bob-->Alice: I am good thanks!
```

#### Checkbox

You can use `- [ ]` and `- [x]` to create checkboxes, for example:

- [x] Item1
- [ ] Item2
- [ ] Item3





# PHPフレームワークの理解を深める

- 情報を集め方
- FWを見定め方
- FWの考え方
- 動かし方

## 情報を手に入れる

1. [PHP The Right Way](https://leanpub.com/phptherightway/)
    1. [要約した](000_PHP_RIght_Way.md?at=master)
1. [Googleで検索してみる](https://www.google.co.jp/search?client=ubuntu&channel=fs&q=php+framework&ie=utf-8&oe=utf-8&hl=ja)
1. [GithubでPHPとMostStarで検索してみる](https://github.com/search?l=PHP&o=desc&q=php&s=stars&type=Repositories&utf8=%E2%9C%93)
1. [PHP Framework](https://github.com/codeguy/php-the-right-way/wiki/Frameworks)
1. [PHPカンファレンスに参加する](http://phpcon.php.gr.jp/2015/)

## FW選定

### 主流だと思うFW

- [CakePHP](http://cakephp.org/)
- [CodeIgniter](http://www.codeigniter.com/)
- [FuelPHP](http://fuelphp.com/)
- [Lalavel](http://laravel.com/)

### 早ければよいのか?

- [2015年最新PHPフレームワーク（9つ）のベンチマーク](http://blog.a-way-out.net/blog/2015/03/26/php-framework-benchmark/)
- [もっと最新版](https://github.com/kenjis/php-framework-benchmark)

通常のフレームワークはPHP言語を利用して開発されている。
Phalconはネイティブモジュールと呼ばれるCでコンパイルして直接PHPから呼び出して利用しているため早い。
しかし、PHP7ではCで記述されているネイティブモジュールが一新されたため、直接置き換えただけではモジュールでは動作しない。
対応はまだといった状況。

早ければいいってもんじゃない。フレームワークの仕組み、中身、将来的に使えるかを見定めて検討すること。 とはいいつつもPHP5はまだ2-3年は主流のはず。

### 将来性

- GitHubの開発頻度をみよう
    - Commit数
    - 関わってる人数
    - starやwatchしている人
- ドキュメント類

日本だけで盛り上がっているFrameworkなど動向に注意すること


- [PHPカンファレンス2014 タイムテーブル](http://phpcon.php.gr.jp/w/2014/)
- [PHPカンファレンス2013 タイムテーブル](http://phpcon.php.gr.jp/w/2013/)
- [PHPカンファレンス2012 タイムテーブル](http://phpcon.php.gr.jp/w/2012/timetable/)



### 動向

- 2013年ぐらいがFWの話題が出ていたけど、その後はだいぶ落ち着いた感じに
- 2014年のタイムテーブルにはほとんどFWの話題がない -> PHP7の話題がちらほら
- 下火になった?
    - 使うのが当たり前
    - Frontendにシフト中
    - PHP7対応前の嵐の前の静けさ
- これから
    - 純粋なPHPで構築されているFW
    - RESTコントローラなどFrontendと親和性の高い
    - PHP7ですぐに動かせるもの


### FWの概念

- [PHPフレームワークとは](http://www.phppro.jp/article/framework/framework.php)
- [フラットなPHPからフレームワークへ](http://www.slideshare.net/brtriver/php-14295877)
- [フレームワークを使うべき 3 つの理由](http://www.slideshare.net/KenichiMukai/conferencekphpugjp20140)


### FW共通の概念

大体どのFWでも実装されていること

- ビューとロジックを分割
- HTMLを出力する仕組みとPHPのロジックを書くソースはだいたい別
- SQL文を投げる部分が予め簡素化された関数で提供されている

### 新しいFWで困ったら

- どのFWでもインストールができていればHello Worldぐらいは出力している
- ドキュメント類を確認すること
- まずはそこを探して少しずつ試してみること
- 僕らが編集する場所と変えてはいけないフレームワークのコア部分を見定めること
- どうしてもわからない場合はindex.phpからたどること   


## とりあえず動かしてみたい

Build In Serverを使え

FuelPHP

```shell
$ wget http://fuelphp.com/files/download/34 -o fuel.zip
$ unzip fuel.zip
$cd INSTALL_DIR/public
$ php -S 127.0.0.1:9000
```

CodeIgniter

```shell
$ wget https://github.com/bcit-ci/CodeIgniter/archive/3.0.1.zip
$ unzip 3.0.1.zip
$ cd Codeigniter3.0.1
$ php -S 127.0.0.1:9000
```

[http://localhost:9000/](http://localhost:9000) を開く 

## 本番環境に近い検証環境作成方法

- 仮想環境にてLinuxをインストール
- サーバのパッケージを入れる前にスナップショットを取る
- FrameWorkインストールする & 手順をメモる
- 実際にソースを書いてみる(できればDBを利用して出し入れ・表示を行うところまで)
- 苦労したところ、簡単だったところなどの結果を味わう
- snapshotをもどす





# 研修資料目次

- Web開発を行う開発者向け
    - ドットインストールにて基礎を学習
    - Webシステムの開発
    - サーバ構築・管理・アプリインストール

## 研修の前に

- [Markdownの書き方](000_howto_markdown.md?at=master)
    - メモがあればmarkdown方式でメモを取る
    - エディタ [Haroopad](http://pad.haroopress.com/)


----

## Web開発に必要なドットインストール 項目


### 環境構築

- [開発環境構築(windows)](http://dotinstall.com/lessons/basic_localdev_win)
- [開発環境構築(mac)](http://dotinstall.com/lessons/basic_localdev_mac)
- [git](http://dotinstall.com/lessons/basic_git)

### フロント系

- [HTML](http://dotinstall.com/lessons/basic_html_v3)
- [CSS](http://dotinstall.com/lessons/basic_css_v3)
- [javascript](http://dotinstall.com/lessons/basic_javascript_v2)
- [jQuery](http://dotinstall.com/lessons/basic_jquery_v2)

### バックエンド系

- [PHP(基礎編)](http://dotinstall.com/lessons/basic_php_v2)
- [MySQL](http://dotinstall.com/lessons/basic_mysql_v2)
- [mongoDB](http://dotinstall.com/lessons/basic_mongodb_v2)

### インフラ系

- [AWS入門](http://dotinstall.com/lessons/basic_aws)
- [UNIXコマンド](http://dotinstall.com/lessons/basic_unix)
- [VPS入門](http://dotinstall.com/lessons/basic_sakura_vps)

----


## Linuxサーバ管理

- [Linuxインストール](001_Linuxインストール.md?at=master)
- [UNIX基礎知識](002_Unix基礎知識.md)
- [サーバ管理コマンド](004_サーバ管理コマンド.md?at=master)
- [ネットワーク管理コマンド](005_ネットワーク系コマンド.md?at=master)
- [wordpressインストール](003_Webサーバ構築_wordpress.md?at=master)

## 開発管理

- [ソースコード管理 Git](100_Git.md?at=master)

## 課題

- [掲示板課題](999_研修課題_01.md?at=master)


## 応用

- [PHP Right Way まとめ](000_PHP_RIght_Way.md?at=master)
- [チーム開発の心得](000_チーム開発の心得.md)

## 関連資料

- [Web Develop Survey (2015-07) Webに関するシェア調査](000_WebDevelopSurvey.md?at=master)
- [オープンソースとは](999_オープンソース.md?at=master)


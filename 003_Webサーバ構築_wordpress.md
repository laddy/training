# webサーバ構築

@(010_Tech)[Webサーバ|研修資料|Marxico]

前提
- ネットワークに繋がったコマンドラインで操作できるUbuntuがあること

## Linux(Ubuntu) + Apache + MySQL + PHP

## Login
セットアップ時に設定したログイン情報を入力

    login: _
    Password: _

### UpdateServer apt-get 

```shell
$ sudo apt-get update
$ sudo apt-get upgrade
```

### Install Apache

```shell
$ sudo apt-get install apache2
```

### Install PHP

```shell
$ sudo apt-get install php5 libapache2-mod-php5 php5-mysql php5-gd php5-xmlrpc php5-curl php5-mcrypt
```

### Install MySQL

```shell
$ sudo apt-get install mysql-server
```


## Wordpress Install

### DB Setup

```shell
$ mysql -u root -e 'create database wordpress' -p
```
    
### Download WordPress

```shell
$ wget https://ja.wordpress.org/latest-ja.tar.gz
```

### MoveWordPress

```shell
$ tar xvzf latest-ja.tar.gz
$ sudo mv wordpress /var/www/html
```

### Edit Setting files

```shell
$ sudo cp wp-config-sample.php wp-config.php
```

### GroupSetting

```shell
$ sudo chown -R www-data:www-data /var/www
```

## Wrodpress Setup

[http://localhost/](http://localhost/)

Setting Admin Password
![Alt text](./WordPress › インストール 2015-07-21 13-37-20.jpg)

WordPress Admin Area
![Alt text](./ダッシュボード ‹ WordpressTest — WordPress 2015-07-21 13-40-31.jpg)